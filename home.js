// Node Predefined Modules
const fs = require("fs");
const os = require("os");
const path = require("path");

// 3rd Party Modules
const express = require("express");

// Custom modules by user


// Cutom Routers for Server


// configuring Application
const app_configuration = JSON.parse(fs.readFileSync("config.json"))
const app = express();
app.use(express.static("static"));

// Function to be used in other places
const server_function = (request, response) => {
    console.log("Server Started @port :" + app_configuration.server.port);
};

const get_file = function(path, response, mime_type){
    fs.readFile(path, function(err, contents) {
        if(!err){
            response.setHeader("Content-Length", contents.length);
			if (mime_type != undefined) {
				response.setHeader("Content-Type", mimeType);
			}
			response.statusCode = 200;
			response.end(contents);
        } else {
            response.writeHead(500);
            response.end();
        }
    });
};

// Tells the server to listen to the given port
app.listen(app_configuration.server.port, server_function);

// Serving a home page for the users
app.get("/", function(request, response, next){
    // request.sen
    let filename = "index.html";
    let connecting_path ="\\static\\html";
    let complete_path = path.join(__dirname, connecting_path, filename);
    fs.exists(complete_path, function(exists){
        console.log("File: " + complete_path + " " + exists);
        if(exists){
            get_file(complete_path, response, undefined);
        } else {

        }
    });
});

// Routing for Application
